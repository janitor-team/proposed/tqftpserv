tqftpserv (1.0-4) unstable; urgency=medium

  [ Dmitry Baryshkov ]
  * d/patches: don't call dirname second time
    dirname modifies passed argument, so don't call it again. Instead
    concatenating firmware_value would result in the proper path being
    constructed.
    (Closes: #1030973)

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 10 Feb 2023 09:02:00 +0100

tqftpserv (1.0-3) unstable; urgency=medium

  * d/patches: lookup firmware files under /lib/firmware/updates
    `/lib/firmware/updates` is a legitimate location for firmware files,
    `tqftpserv` should look there as well.

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 09 Feb 2023 19:53:43 +0100

tqftpserv (1.0-2) unstable; urgency=medium

  * d/rules: install service file under `/lib/systemd`
    `debhelper` looks for service files under `/lib/systemd`, but ignores
    those installed under `/usr/lib/systemd`, so we need to install our
    service file to the former location to have it auto-enabled on install.

 -- Arnaud Ferraris <aferraris@debian.org>  Wed, 11 Jan 2023 00:16:00 +0100

tqftpserv (1.0-1) unstable; urgency=medium

  * New upstream version 1.0
  * d/control: bump Standards-Version, no change required

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 03 Jan 2023 10:39:18 +0100

tqftpserv (0.0+git20200207-2) unstable; urgency=medium

  * Source-only upload to allow transitioning to testing

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 04 Aug 2022 13:19:32 +0200

tqftpserv (0.0+git20200207-1) unstable; urgency=medium

  * Initial Debian packaging (Closes: #1016013)

 -- Arnaud Ferraris <aferraris@debian.org>  Mon, 25 Jul 2022 13:49:30 +0200
